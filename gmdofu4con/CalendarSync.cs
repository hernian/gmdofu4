﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using gmdofu4common;

namespace gmdofu4con
{
    /// <summary>
    /// 処理を行うスレッドがメインスレッドから他に移らないように注意せよ。
    /// とくにGoogleCalendarProviderの処理は非同期なのでWait()で待ち合わせること。awaitはダメ。
    /// </summary>
    class CalendarSync
    {
        private const int YEARS_RANGE = 3;

        private TraceLog _traceLog;
        private Config _config;
        private OutlookCalendarProvider _outlookCalendarProvider;
        private GoogleCalendarProvider _googleCalendarProvider;
        private Dictionary<Guid, GatheredCalendarItems> _gatheredCalendarItemDict = new Dictionary<Guid, GatheredCalendarItems>();

        public CalendarSync()
        {
            _traceLog = TraceLog.TheInstance;
            _config = Config.Load();

            // Outlookが起動していないときは例外が投げられ、続く処理が出来ないのでGoogleより先に初期化してみる。
            _outlookCalendarProvider = new OutlookCalendarProvider(_config);
            _googleCalendarProvider = GoogleCalendarProvider.CreateAsync(_config).Result;
        }

        public void SyncCalendars()
        {

            var start = DateTime.Today;
            var end = start.AddYears(3);

            var calendarItemsGoogle = _googleCalendarProvider.GetCalendarItemsAsync(start, end).Result;
            var calendarItemsOutlook = _outlookCalendarProvider.GetCalendarItems(start, end);

            foreach (var ci in calendarItemsGoogle)
            {
                ci.Flag = false;
            }
            foreach (var ci in calendarItemsOutlook)
            {
                ci.Flag = false;
            }

            // 1st. GUIDで一致するものを集める
            AddCalendarItemsWithGuid(calendarItemsGoogle, GatheredCalendarItems.SourceType.Google);
            AddCalendarItemsWithGuid(calendarItemsOutlook, GatheredCalendarItems.SourceType.Outlook);

            // 2nd. 名前、場所、開始・終了時刻で一致するものを集める
            AddCalendarItemsRemain(calendarItemsGoogle, GatheredCalendarItems.SourceType.Google);
            AddCalendarItemsRemain(calendarItemsOutlook, GatheredCalendarItems.SourceType.Outlook);

            // 集めたものを同期する
            foreach (var gatheredCalendarItems in _gatheredCalendarItemDict.Values)
            {
                SynchronizeGatheredCalendarItems(gatheredCalendarItems, calendarItemsGoogle, calendarItemsOutlook);
            }

#if true
            // outlook予定表より先にgoogle予定を削除する。
            // google予定表は削除しても削除フラグが付くだけでデータそのものは消えない。
            // outlook予定表は削除したら単純に消えてしまう。
            // google予定表を削除してから1秒後にOutlook予定表を削除することで、同期前にoutlook予定表が削除されたと誤認識しなくなる。
            _googleCalendarProvider.ApplyAsync(calendarItemsGoogle).Wait();
            System.Threading.Thread.Sleep(1000);
            _outlookCalendarProvider.Apply(calendarItemsOutlook);
#endif
        }

        private void AddCalendarItemsWithGuid(IEnumerable<CalendarItem> srcs, GatheredCalendarItems.SourceType sourceType)
        {
            var calendarItemsWithGuid = srcs.Where(ci => ((ci.Flag == false) && (ci.SyngronizeGuid != Guid.Empty)));
            foreach (var calendarItem in calendarItemsWithGuid)
            {
                var guid = calendarItem.SyngronizeGuid;
                var gatheredCalendarItems = default(GatheredCalendarItems);
                var rg = _gatheredCalendarItemDict.TryGetValue(guid, out gatheredCalendarItems);
                if (rg == false)
                {
                    gatheredCalendarItems = new GatheredCalendarItems(guid);
                    _gatheredCalendarItemDict[guid] = gatheredCalendarItems;
                }
                var ra = gatheredCalendarItems.AddItem(calendarItem, sourceType);
                if (ra)
                {
                    calendarItem.Flag = true;
                }
            }
        }

        private void AddCalendarItemsRemain(IEnumerable<CalendarItem> srcs, GatheredCalendarItems.SourceType sourceType)
        {
            var calendarItemsRemain = srcs.Where(ci => (ci.Flag == false));
            foreach (var calendarItem in calendarItemsRemain)
            {
                var founds = FindGatheredCalendarItems(calendarItem);
                var r = AddCalendarItem(founds, calendarItem, sourceType);
                if (r == false)
                {
                    var guid = Guid.NewGuid();
                    var gatheredCalendarItemsNew = new GatheredCalendarItems(guid);
                    gatheredCalendarItemsNew.AddItem(calendarItem, sourceType);
                    _gatheredCalendarItemDict[guid] = gatheredCalendarItemsNew;
                }
                calendarItem.Flag = true;
            }
        }

        private bool IsCalendarItemUpdateRequired(Guid guid, CalendarItem key, CalendarItem calendarItem)
        {
            if (calendarItem == null)
            {
                return true;
            }
            if (calendarItem.SyngronizeGuid != guid)
            {
                return true;
            }
            if (calendarItem.Name != key.Name)
            {
                return true;
            }
            if (calendarItem.Start != key.Start)
            {
                return true;
            }
            if (calendarItem.End != key.End)
            {
                return true;
            }
            if (calendarItem.AllDayEvent != key.AllDayEvent)
            {
                return true;
            }
            if (calendarItem.Cancelled != key.Cancelled)
            {
                return true;
            }
            if (calendarItem.Location != key.Location)
            {
                return true;
            }
            if (calendarItem.Body != key.Body)
            {
                return true;
            }
            return false;
        }

        private void LogGatheredCalendarItems(GatheredCalendarItems gatheredCalendarItems)
        {
            var guid = gatheredCalendarItems.Guid;
            var calendarItemGoogle = gatheredCalendarItems.GoogleItem;
            var calendarItemOutlook = gatheredCalendarItems.OutlookItem;
            _traceLog.WriteLine("  同期イベント集合 GUID:{0}", guid.ToString("B"));
            if (calendarItemGoogle != null)
            {
                _traceLog.WriteLine("    [Google ] 名前:{0} 開始時刻:{1} 終了時刻:{2} GUID:{3} キャンセル:{4,-5} 場所:{5}",
                                    calendarItemGoogle.Name,
                                    calendarItemGoogle.Start.ToString("u"),
                                    calendarItemGoogle.End.ToString("u"),
                                    calendarItemGoogle.SyngronizeGuid.ToString("B"),
                                    calendarItemGoogle.Cancelled,
                                    calendarItemGoogle.Location);
            }
            if (calendarItemOutlook != null)
            {
                _traceLog.WriteLine("    [Outlook] 名前:{0} 開始時刻:{1} 終了時刻:{2} GUID:{3} キャンセル:{4,-5} 場所:{5}",
                                    calendarItemOutlook.Name,
                                    calendarItemOutlook.Start.ToString("u"),
                                    calendarItemOutlook.End.ToString("u"),
                                    calendarItemOutlook.SyngronizeGuid.ToString("B"),
                                    calendarItemOutlook.Cancelled,
                                    calendarItemOutlook.Location);
            }
        }

        private void SynchronizeGatheredCalendarItems(GatheredCalendarItems gatheredCalendarItems, List<CalendarItem> calendarItemsGoogle, List<CalendarItem> calendarItemsOutlook)
        {
            var guid = gatheredCalendarItems.Guid;
            var key = gatheredCalendarItems.Key;

            var changed = false;

            if (key.Cancelled == false)
            {
                var a = new CalendarItem[]
                {
                    gatheredCalendarItems.GoogleItem,
                    gatheredCalendarItems.OutlookItem
                };
                foreach (var calendarItem in a)
                {
                    if (IsCalendarItemUpdateRequired(guid, key, calendarItem))
                    {
                        changed = true;
                        break;
                    }
                }
            }
            else
            {
                foreach (var calendarItem in gatheredCalendarItems.AllItems)
                {
                    if (calendarItem.Cancelled == false)
                    {
                        changed = true;
                    }
                }
            }

            if (changed)
            {
                LogGatheredCalendarItems(gatheredCalendarItems);
            }


            if (key.Cancelled == false)
            {
                if (gatheredCalendarItems.GoogleItem == null)
                {
                    var calendarItemNew = new CalendarItem();
                    gatheredCalendarItems.AddItem(calendarItemNew, GatheredCalendarItems.SourceType.Google);
                    calendarItemsGoogle.Add(calendarItemNew);
                }
                if (gatheredCalendarItems.OutlookItem == null)
                {
                    var calendarItemNew = new CalendarItem();
                    gatheredCalendarItems.AddItem(calendarItemNew, GatheredCalendarItems.SourceType.Outlook);
                    calendarItemsOutlook.Add(calendarItemNew);
                }
                foreach (var calendarItem in gatheredCalendarItems.AllItems)
                {
                    SynchronizeItem(guid, key, calendarItem);
                }
            }
            else
            {
                // 予定のキャンセルは、マッチした全項目に対して行う。
                foreach (var calendarItem in gatheredCalendarItems.AllItems)
                {
                    if (calendarItem.Cancelled == false)
                    {
                        calendarItem.Cancelled = true;
                        calendarItem.Changed = true;
                    }
                }
            }
        }

        private void SynchronizeItem(Guid guid, CalendarItem key, CalendarItem calendarItem)
        {
            var changed = false;
            if (calendarItem.SyngronizeGuid != guid)
            {
                calendarItem.SyngronizeGuid = guid;
                changed = true;
            }
            if (calendarItem.Name != key.Name)
            {
                calendarItem.Name = key.Name;
                changed = true;
            }
            if (calendarItem.Start != key.Start)
            {
                calendarItem.Start = key.Start;
                changed = true;
            }
            if (calendarItem.End != key.End)
            {
                calendarItem.End = key.End;
                changed = true;
            }
            if (calendarItem.AllDayEvent != key.AllDayEvent)
            {
                calendarItem.AllDayEvent = key.AllDayEvent;
                changed = true;
            }
            if (calendarItem.Cancelled != key.Cancelled)
            {
                calendarItem.Cancelled = key.Cancelled;
                changed = true;
            }
            if (calendarItem.Location != key.Location)
            {
                calendarItem.Location = key.Location;
                changed = true;
            }
            if (calendarItem.Body != key.Body)
            {
                calendarItem.Body = key.Body;
                changed = true;
            }
            calendarItem.Changed = changed;
        }

        private IEnumerable<GatheredCalendarItems> FindGatheredCalendarItems(CalendarItem calendarItem)
        {
            var kvs = from kv in _gatheredCalendarItemDict
                    where MatchCalendarItems(kv.Value.Key, calendarItem)
                    select kv.Value;
            return kvs;
        }

        private bool AddCalendarItem(IEnumerable<GatheredCalendarItems> gatheredCalendarItems, CalendarItem calendarItem, GatheredCalendarItems.SourceType sourceType)
        {
            foreach (var gci in gatheredCalendarItems)
            {
                var r = gci.AddItem(calendarItem, sourceType);
                if (r)
                {
                    return true;
                }
            }
            return false;
        }

        private bool MatchCalendarItems(CalendarItem lhs, CalendarItem rhs)
        {
            if (lhs.Name != rhs.Name)
            {
                return false;
            }
            if (lhs.AllDayEvent != rhs.AllDayEvent)
            {
                return false;
            }
            if (lhs.Start != rhs.Start)
            {
                return false;
            }
            if (lhs.End != rhs.End)
            {
                return false;
            }
            return true;
        }
#if false
        /// <summary>
        /// Outlookを操作してカレンダーアイテムを複製するとSynchronizeGuidがコピーされて重複が発生する。
        /// そのままでは、コピー前・コピー後の2つのカレンダーアイテムが同期してしまうが、それはコピーした人の目的とは異なるだろう。
        /// コピーした人は、コピー前・コピー後のいずれかの日時を変更するのが普通の使い方である。
        /// なので、SychronizeGuidが重複していた場合、新しい方を新規（SynchronizeGuidがEmpty）とする。
        /// </summary>
        /// <param name="calendarItems">SyncronizeGuidの重複を修正する対象</param>
        /// <param name="providerName">プロバイダ名</param>
        private void FixDuplicatedCalendarItems(List<CalendarItem> calendarItems, string providerName)
        {
            var firstTime = true;
            var dict = new Dictionary<Guid, CalendarItem>();
            foreach (var calendarItem in calendarItems)
            {
                var guidKey = calendarItem.SyngronizeGuid;
                if (guidKey == Guid.Empty)
                {
                    continue;
                }
                var calendarItemTemp = default(CalendarItem);
                var r = dict.TryGetValue(guidKey, out calendarItemTemp);
                if (r)
                {
                    if (firstTime)
                    {
                        firstTime = false;
                        _traceLog.WriteLine("[{0}]重複の修正[開始]", providerName);
                    }
                    if (calendarItemTemp.LastModified < calendarItem.LastModified)
                    {
                        _traceLog.WriteCalendarItem("  [重複A]", calendarItemTemp);
                        _traceLog.WriteCalendarItem("  [重複B]", calendarItem);
                        calendarItem.SyngronizeGuid = Guid.Empty;
                    }
                    else
                    {
                        _traceLog.WriteCalendarItem("  [重複A]", calendarItem);
                        _traceLog.WriteCalendarItem("  [重複B]", calendarItemTemp);
                        calendarItemTemp.SyngronizeGuid = Guid.Empty;
                        dict[guidKey] = calendarItem;
                    }
                }
                else
                {
                    dict[guidKey] = calendarItem;
                }
            }
            if (firstTime == false)
            {
                _traceLog.WriteLine("[{0}]重複の修正[終了]", providerName);
            }
        }
#endif
    }
}

