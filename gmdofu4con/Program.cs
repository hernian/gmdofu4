﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using gmdofu4common;

namespace gmdofu4con
{
    class Program
    {
        /// <summary>
        /// メインスレッドはMicrosoft.Office.Interop.Outlookを呼び出すため STAThreadであること。
        /// Task<T>をawaitで待ち合わせると実行しているスレッドが変わってしまうため、 Wait()で待ち合わせること。
        /// Wait()で待ち合わせたTask<T>内部でawaitを使うことは問題ない。
        /// </summary>
        /// <param name="args"></param>
        [STAThread]
        static void Main(string[] args)
        {
            using (var mutex = new Mutex(false, "gmdofu4con$mutex"))
            {
                try
                {
                    mutex.WaitOne(0);
                    DoMain();
                }
                finally
                {
                    mutex.ReleaseMutex();
                }
            }
        }

        static void DoMain()
        {
            var traceLog = default(TraceLog);
            try
            {
                // ログの出力先も含めてここでディレクトリを作成するので、TraceLog.TheInstanceより先に呼び出すこと。
                FileUtil.CreateFileDir();

                traceLog = TraceLog.TheInstance;
                traceLog.WriteLine("起動");

                LogVersions(traceLog);
                var calendarSync = new CalendarSync();
                calendarSync.SyncCalendars();

                traceLog.WriteLine("正常終了");
            }
            catch (Exception ex)
            {
                if (traceLog != default(TraceLog))
                {
                    traceLog.WriteLine(ex);
                    traceLog.WriteLine("処理中断");
                }
                Environment.Exit(1);
            }
        }

        static void LogVersions(TraceLog traceLog)
        {
            var fileVers = gmdofu4common.Version.GetFileVersions();
            var sb = new StringBuilder();
            sb.Append("Versions: ");
            var firstTime = true;
            foreach (var fileVer in fileVers)
            {
                var fileName = System.IO.Path.GetFileName(fileVer.FileName);
                if (firstTime == false)
                {
                    sb.Append(", ");
                }
                sb.AppendFormat("{0}({1})", fileName, fileVer.FileVersion);
                firstTime = false;
            }
            traceLog.WriteLine(sb.ToString());
        }
    }
}
