﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Outlook = Microsoft.Office.Interop.Outlook;
using gmdofu4common;

namespace gmdofu4con
{
    class OutlookCalendarProvider
    {
        private const string FILTER_DATE_PATTERN = "yyyy/MM/dd";
        private const string USER_PROPERTY_NAME_GO_CALENDAR_SYNC = "gmdofu3guid";

        private TraceLog _log;
        private Config _config;
        private Outlook.Application _app;
        private Outlook.NameSpace _nameSpace;
        private Outlook.MAPIFolder _mapiFolder;

        public OutlookCalendarProvider(Config config)
        {
            _log = TraceLog.TheInstance;
            _config = config;

            if (OutlookProcess.Count == 0)
            {
                throw new Exception("Outlookが起動していない");
            }
            // 既存のOutlookプロセスに接続する
            _app = Marshal.GetActiveObject("Outlook.Application") as Outlook.Application;
            _nameSpace = _app.GetNamespace("MAPI");
            _mapiFolder = _nameSpace.GetDefaultFolder(Outlook.OlDefaultFolders.olFolderCalendar);

            var connMode = _nameSpace.ExchangeConnectionMode;
            if (connMode == Outlook.OlExchangeConnectionMode.olDisconnected)
            {
                throw new Exception("Outlookはサーバーから切断されている");
            }
            if (_nameSpace.Offline)
            {
                throw new Exception("Outlookはオフライン");
            }
        }

        public List<CalendarItem> GetCalendarItems(DateTime start, DateTime end)
        {
            var appoItems = GetAppointments(start, end);

            var list = new List<CalendarItem>();
            foreach (Outlook.AppointmentItem appoItem in appoItems)
            {
                if (appoItem.IsRecurring)
                {
                    continue;
                }
                if (appoItem.MeetingStatus == Outlook.OlMeetingStatus.olMeetingCanceled)
                {
                    continue;
                }
                if ((appoItem.End < start) || (end <= appoItem.Start))
                {
                    continue;
                }
                var calendarItem = CreateCalendarItem(appoItem);
                list.Add(calendarItem);
            }

            OutlookCalendarCache.Merge(list);
            return list;
        }

        public void Apply(List<CalendarItem> list)
        {
            _log.WriteLine("Outlook更新開始");
            foreach (var calendarItem in list)
            {
                try
                {
                    if (calendarItem.Changed == false)
                    {
                        continue;
                    }
                    if (calendarItem.Cancelled)
                    {
                        DeleteItem(calendarItem);
                    }
                    else if (calendarItem.ID == string.Empty)
                    {
                        CreateItem(calendarItem);
                    }
                    else
                    {
                        UpdateItem(calendarItem);
                    }
                }
                catch (Exception ex)
                {
                    _log.WriteLine(ex);
                }
            }
            var now = DateTime.Now;
            OutlookCalendarCache.Save(list, now);

            _log.WriteLine("Outlook更新完了");
        }

        public Outlook.Items GetAppointments(DateTime start, DateTime end)
        {
            var strStart = start.ToString(FILTER_DATE_PATTERN);
            var strEnd = end.ToString(FILTER_DATE_PATTERN);
            var filter = string.Format("[Start] >= '{0}' AND [Start] <= '{1}'", strStart, strEnd);
            var items = _mapiFolder.Items.Restrict(filter);
            return items;
        }

        private Guid GetGOCalendarSyncGUID(Outlook.AppointmentItem appoItem)
        {
            var prop = appoItem.UserProperties.Find(USER_PROPERTY_NAME_GO_CALENDAR_SYNC);
            if (prop == null)
            {
                return Guid.Empty;
            }
            if (prop.Value == null)
            {
                return Guid.Empty;
            }
            var strValue = (string)prop.Value;
            var guid = default(Guid);
            var r = Guid.TryParse(strValue, out guid);
            if (r == false)
            {
                return Guid.Empty;
            }
            return guid;
        }
        
        private void CreateItem(CalendarItem calendarItem)
        {
            _log.WriteCalendarItem("新規", calendarItem);
            var appoItem = _mapiFolder.Items.Add() as Outlook.AppointmentItem;
            UpdateAppointmentItem(appoItem, calendarItem);
        }

        private void DeleteItem(CalendarItem calendarItem)
        {
            _log.WriteCalendarItem("削除", calendarItem);
            var appoItem = calendarItem.Tag as Outlook.AppointmentItem;
            if (appoItem == null)
            {
                // あるはずのアポイントメントがないので無視する
                return;
            }
            appoItem.Delete();
        }
 
        private void UpdateItem(CalendarItem calendarItem)
        {
            var appoItem = calendarItem.Tag as Outlook.AppointmentItem;
            if (appoItem == null)
            {
                // 更新するべきアポイントメントがないので無視する
                return;
            }
            var calendarItemOrg = CreateCalendarItem(appoItem);
            _log.WriteCalendarItem("変更前", calendarItemOrg);
            _log.WriteCalendarItem("変更後", calendarItem);
            UpdateAppointmentItem(appoItem, calendarItem);
        }

        public CalendarItem CreateCalendarItem(Outlook.AppointmentItem appoItem)
        {
            var calendarItem = new CalendarItem();
            calendarItem.ID = appoItem.EntryID;
            calendarItem.SyngronizeGuid = GetGOCalendarSyncGUID(appoItem);
            calendarItem.Name = appoItem.Subject;
            calendarItem.AllDayEvent = appoItem.AllDayEvent;
            calendarItem.Cancelled = false;
            calendarItem.Start = appoItem.Start;
            calendarItem.End = appoItem.End;
            calendarItem.LastModified = appoItem.LastModificationTime;
            calendarItem.Location = (appoItem.Location == null) ? string.Empty: appoItem.Location;
            calendarItem.Body = (appoItem.Body == null) ? string.Empty: appoItem.Body;
            calendarItem.Tag = appoItem;
            return calendarItem;
        }

        void  UpdateAppointmentItem(Outlook.AppointmentItem appoItem, CalendarItem calendarItem)
        {
            if (EqualsAppointmentPropertyString(appoItem.Subject, calendarItem.Name) == false)
            {
                appoItem.Subject = calendarItem.Name;
            }
            if (appoItem.AllDayEvent != calendarItem.AllDayEvent)
            {
                appoItem.AllDayEvent = calendarItem.AllDayEvent;
            }
            if (appoItem.Start != calendarItem.Start)
            {
                appoItem.Start = calendarItem.Start;
            }
            if (appoItem.End != calendarItem.End)
            {
                appoItem.End = calendarItem.End;
            }
            if (EqualsAppointmentPropertyString(appoItem.Location, calendarItem.Location) == false)
            {
                appoItem.Location = calendarItem.Location;
            }
            if (EqualsAppointmentPropertyString(appoItem.Body, calendarItem.Body) == false)
            {
                appoItem.Body = calendarItem.Body;
            }
            var userProp = appoItem.UserProperties[USER_PROPERTY_NAME_GO_CALENDAR_SYNC];
            if (userProp != null)
            {
                var guidStr = calendarItem.SyngronizeGuid.ToString();
                if (userProp.Value != guidStr)
                {
                    userProp.Value = guidStr;
                }
            }
            else
            {
                var p = appoItem.UserProperties.Add(USER_PROPERTY_NAME_GO_CALENDAR_SYNC, Outlook.OlUserPropertyType.olText);
                p.Value = calendarItem.SyngronizeGuid.ToString();
            }
            appoItem.Save();
        }

        private bool EqualsAppointmentPropertyString(string propertyStr, string newValue)
        {
            if (string.IsNullOrEmpty(propertyStr) && string.IsNullOrEmpty(newValue))
            {
                return true;
            }
            return propertyStr == newValue;
        }
    }
}
