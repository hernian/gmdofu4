﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using gmdofu4common;

namespace gmdofu4con
{
    class OutlookCalendarCache
    {
        private const string FILENAME_ITEMS_CACHE = "cache.xml";

        public List<CalendarItem> _list = new List<CalendarItem>();

        public static void Merge(List<CalendarItem> listDst)
        {
            var dict = CreateDictionary(listDst);
            var listCache = Load();
            foreach (var item in listCache.Where(i => (dict.ContainsKey(i.SyngronizeGuid) == false)))
            {
                item.Cancelled = true;
                listDst.Add(item);
            }
        }

        public static void Save(List<CalendarItem> listSrc, DateTime lastModified)
        {
            // 削除された予定を除いて保存する
            var listSave = new List<CalendarItem>();
            foreach (var itemSrc in listSrc)
            {
                if (itemSrc.Cancelled)
                {
                    continue;
                }
                var itemNew = CreateCalendarItem(itemSrc, lastModified);
                listSave.Add(itemNew);
            }

            var fileName = FileUtil.GetFilePath(FILENAME_ITEMS_CACHE);
            var encoding = new UTF8Encoding(false);
            var serializer = new XmlSerializer(typeof(List<CalendarItem>));
            using (var sw = new StreamWriter(fileName, false, encoding))
            {
                serializer.Serialize(sw, listSave);
            }
        }

        private static List<CalendarItem> Load()
        {
            var list = default(List<CalendarItem>);
            try
            {
                var fileName = FileUtil.GetFilePath(FILENAME_ITEMS_CACHE);
                var encoding = new UTF8Encoding(false);
                var serializer = new XmlSerializer(typeof(List<CalendarItem>));
                using (var sr = new StreamReader(fileName, encoding))
                {
                    list = (List<CalendarItem>)serializer.Deserialize(sr);
                }
            }
            catch (Exception)
            {
                list = new List<CalendarItem>();
            }
            return list;
        }

        private static CalendarItem CreateCalendarItem(CalendarItem itemSrc, DateTime lastModified)
        {
            var itemNew = new CalendarItem();
            itemNew.ID = itemSrc.ID;
            itemNew.SyngronizeGuid = itemSrc.SyngronizeGuid;
            itemNew.Name = itemSrc.Name;
            itemNew.Start = itemSrc.Start;
            itemNew.End = itemSrc.End;
            itemNew.LastModified = lastModified;
            itemNew.AllDayEvent = itemSrc.AllDayEvent;
            itemNew.Cancelled = itemSrc.Cancelled;
            itemNew.Location = itemSrc.Location;
            itemNew.Body = string.Empty;
            itemNew.Changed = false;
            itemNew.Tag = null;
            return itemNew;
        }

        private static Dictionary<Guid, CalendarItem> CreateDictionary(List<CalendarItem> list)
        {
            var dict = new Dictionary<Guid, CalendarItem>();
            foreach (var item in list)
            {
                dict[item.SyngronizeGuid] = item;
            }
            return dict;
        }

    }
}
