﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using gmdofu4common;

namespace gmdofu4con
{
    class GatheredCalendarItems
    {
        public enum SourceType
        {
            Google,
            Outlook
        }

        private readonly Guid _guid;
        private CalendarItem _key = null;
        private List<CalendarItem> _allCalendarItems = new List<CalendarItem>();
        private Dictionary<SourceType, CalendarItem> _dict = new Dictionary<SourceType, CalendarItem>();

        public GatheredCalendarItems(Guid guid)
        {
            _guid = guid;
        }

        public GatheredCalendarItems()
        {
            _guid = Guid.NewGuid();
        }

        public Guid Guid
        {
            get { return _guid; }
        }

        public CalendarItem Key
        {
            get { return _key; }
        }

        public bool AddItem(CalendarItem calendarItem, SourceType sourceType)
        {
            if (_dict.ContainsKey(sourceType))
            {
                return false;
            }
            _dict[sourceType] = calendarItem;
            _allCalendarItems.Add(calendarItem);           
            if (_key == null)
            {
                _key = calendarItem;
            }
            else
            {
                if (_key.LastModified < calendarItem.LastModified)
                {
                    _key = calendarItem;
                }
            }
            return true;
        }

        public ReadOnlyCollection<CalendarItem> AllItems
        {
            get { return _allCalendarItems.AsReadOnly(); }
        }

        public CalendarItem GoogleItem
        {
            get {
                var calendarItem = default(CalendarItem);
                var r = _dict.TryGetValue(SourceType.Google, out calendarItem);
                return r ? calendarItem : null;
            }
        }

        public CalendarItem OutlookItem
        {
            get
            {
                var calendarItem = default(CalendarItem);
                var r = _dict.TryGetValue(SourceType.Outlook, out calendarItem);
                return r ? calendarItem : null;
            }
        }
    }
}
