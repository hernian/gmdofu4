﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace gmdofu4
{
    class MessageText
    {
        private const string CRLF = "\r\n";

        private TextBox _textBox;

        public MessageText(TextBox textBox)
        {
            _textBox = textBox;
        }

        public void Clear()
        {
            _textBox.Text = string.Empty;
        }

        public void Write(string value)
        {
            var lenPre = _textBox.TextLength;
            _textBox.Select(lenPre, 0);
            _textBox.SelectedText = value;
            var lenPost = _textBox.TextLength;
            _textBox.Select(lenPost, 0);
            _textBox.ScrollToCaret();
        }

        public void Write(string format, params object[] args)
        {
            var value = string.Format(format, args);
            Write(value);
        }

        public void WriteLine(string value)
        {
            Write(value + CRLF);
        }

        public void WriteLine(string format, params object[] args)
        {
            var value = string.Format(format, args);
            Write(value + CRLF);
        }
    }
}
