﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace gmdofu4
{
    public partial class FormVersion : Form
    {
        public FormVersion()
        {
            InitializeComponent();
        }

        private async void FormVersion_Load(object sender, EventArgs e)
        {
            await FormUtil.ExceptionHarness(this, async () =>
            {
                var fileVers = await gmdofu4common.Version.GetFileVersionsAsync();
                var firstTime = true;
                var sb = new StringBuilder();
                foreach (var fileVer in fileVers)
                {
                    if (firstTime)
                    {
                        firstTime = false;
                        labelAbout.Text = fileVer.Comments;
                    }
                    var fileName = Path.GetFileName(fileVer.FileName);
                    var lvItem = new ListViewItem();
                    lvItem.Text = fileName;
                    lvItem.SubItems.Add(fileVer.FileVersion);
                    listViewFileVersion.Items.Add(lvItem);
                    await Task.Yield();
                }
            });
        }
    }
}
