﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using gmdofu4common;

namespace gmdofu4
{
    class LogShowAgent
    {
        public static Task ShowLogAsync()
        {
            Action action = () => {
                var spi = new ProcessStartInfo()
                {
                    FileName = TraceLog.TheInstance.LogFileName,
                    UseShellExecute = true
                };
                var process = Process.Start(spi);
                process.Close();
            };
            return Task.Run(action);
        }
    }
}
