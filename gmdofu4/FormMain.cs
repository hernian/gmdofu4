﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using gmdofu4common;

namespace gmdofu4
{
    public partial class FormMain : Form
    {
        private const int MS_DELAY_FIRST_SYNC = 10 * 60 * 1000;         // 初回同期は起動後10分
        private const int MS_INTERVAL_SYNC = 60 * 60 * 1000;            // 同期間隔は1時間

        private CalendarSynchronizerAgent _calendarSynchronizerAgent;
        private Icon[] _icons;
        private int _idxIcon;

        public FormMain()
        {
            InitializeComponent();
            _calendarSynchronizerAgent = new CalendarSynchronizerAgent();
            _icons = new Icon[] 
            {
                Properties.Resources.NotifyIcon,
                Properties.Resources.NotifyIcon2
            };
            _idxIcon = 0;

            timerSync.Interval = MS_DELAY_FIRST_SYNC;
            timerSync.Enabled = true;
        }

        private async void toolStripMenuItemSyncNow_Click(object sender, EventArgs e)
        {
            await SynchronizeCalendarsAsync();
        }

        private async Task SynchronizeCalendarsAsync()
        {
            if (toolStripMenuItemSyncNow.Enabled == false)
            {
                // カレンダーに破滅的破壊をもたらすので同期の二重起動は禁止
                return;
            }
 
            toolStripMenuItemSyncNow.Enabled = false;
            toolStripMenuItemConfig.Enabled = false;
            toolStripMenuItemExit.Enabled = false;
            _idxIcon = 1;
            notifyIcon1.Icon = _icons[_idxIcon];
            timerBlinker.Enabled = true;

            await FormUtil.ExceptionHarness(this, async () =>
            {
                await _calendarSynchronizerAgent.SynchronizeAsync().ConfigureAwait(false);
            });

            timerBlinker.Enabled = false;
            _idxIcon = 0;
            notifyIcon1.Icon = _icons[_idxIcon];
            toolStripMenuItemExit.Enabled = true;
            toolStripMenuItemConfig.Enabled = true;
            toolStripMenuItemSyncNow.Enabled = true;
        }

        private void toolStripMenuItemConfig_Click(object sender, EventArgs e)
        {
            toolStripMenuItemConfig.Enabled = false;
            using (var form = new FormConfig())
            {
                form.ShowDialog(this);
            }
            toolStripMenuItemConfig.Enabled = true;
        }

        private void toolStripMenuItemAbout_Click(object sender, EventArgs e)
        {
            toolStripMenuItemAbout.Enabled = false;
            using (var form = new FormVersion())
            {
                form.ShowDialog(this);
            }
            toolStripMenuItemAbout.Enabled = true;
        }

        private void timerBlinker_Tick(object sender, EventArgs e)
        {
            _idxIcon = (_idxIcon == 0) ? 1 : 0;
            notifyIcon1.Icon = _icons[_idxIcon];
        }

        private void toolStripMenuItemExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private async void timerSync_Tick(object sender, EventArgs e)
        {
            if (timerSync.Interval != MS_INTERVAL_SYNC)
            {
                timerSync.Interval = MS_INTERVAL_SYNC;
            }
            await FormUtil.ExceptionHarness(this, async () =>
            {
                await _calendarSynchronizerAgent.SynchronizeAsync();
            });
        }

        private async void toolStripMenuItemShowLog_Click(object sender, EventArgs e)
        {
            await FormUtil.ExceptionHarness(this, async () =>
            {
                await LogShowAgent.ShowLogAsync();
            });
        }

        private void toolStripMenuItemSyncNow_DropDownOpening(object sender, EventArgs e)
        {

        }

        private void contextMenuStrip1_Opening(object sender, CancelEventArgs e)
        {
            toolStripMenuItemSyncNow.Enabled = (OutlookProcess.Count > 0);
        }
    }
}
