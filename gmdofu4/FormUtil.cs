﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace gmdofu4
{
    class FormUtil
    {
        private const string CRLF = "\r\n";

        public static async Task ExceptionHarness(Form owner, Func<Task> func)
        {
            try
            {
                await func();
            }
            catch (Exception ex)
            {
                ShowException(owner, ex);
            }
        }

        public static void ExceptionHarness(Form owner, Action action)
        {
            try
            {
                action();
            }
            catch (Exception ex)
            {
                ShowException(owner, ex);
            }
        }

        public static void ShowException(Form owner, Exception ex)
        {
            var sb = new StringBuilder();
            while (ex != null)
            {
                sb.Append(ex.Message);
                if (ex.Message.EndsWith(CRLF) == false)
                {
                    sb.Append(CRLF);
                }
            }
            var msg = sb.ToString();
            var caption = owner.Text + " エラー";
            MessageBox.Show(owner, msg, caption, MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
    }
}
