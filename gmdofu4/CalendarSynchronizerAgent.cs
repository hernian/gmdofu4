﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using gmdofu4common;

namespace gmdofu4
{
    class CalendarSynchronizerAgent
    {
        private const string FILENAME_CONSOLE_APP = "gmdofu4con.exe";

        private bool _synchronizing = false;

        public CalendarSynchronizerAgent()
        {
        }

        public async Task SynchronizeAsync()
        {
            if (_synchronizing)
            {
                return;
            }

            try
            {
                _synchronizing = true;
                await Task.Run(() =>
                {
                    var psi = new ProcessStartInfo()
                    {
                        FileName = FileUtil.GetExePathFileName(FILENAME_CONSOLE_APP),
                        UseShellExecute = false,
                        CreateNoWindow = true
                    };
                    using (var process = Process.Start(psi))
                    {
                        process.WaitForExit();
                    }
                });
            }
            finally
            {
                _synchronizing = false;
            }
        }
    }
}
