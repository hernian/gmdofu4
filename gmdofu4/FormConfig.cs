﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using gmdofu4common;

namespace gmdofu4
{
    public partial class FormConfig : Form
    {
        private const int COUNT_RETRY_LOAD = 3;
        private const int MS_INTERVAL_RETRY = 100;
        private const int MS_DEPAY_MESSAGE = 3000;

        private MessageText _messageText;
        private Config _config;

        public FormConfig()
        {
            InitializeComponent();
            _messageText = new MessageText(textBoxMessage);
            
        }

        private async void FormConfig_Load(object sender, EventArgs e)
        {
            var exceptionLoad = default(Exception);
            for (var i = 0; i < COUNT_RETRY_LOAD; ++i)
            {
                try
                {
                    _config = Config.Load();
                    exceptionLoad = default(Exception);
                    break;
                }
                catch (Exception ex)
                {
                    exceptionLoad = ex;
                }
                await Task.Delay(MS_INTERVAL_RETRY);
            }
            if (_config == null)
            {
                _messageText.WriteLine("設定をロードできません");
                return;
            }

            textBoxGoogleCalendarID.Text = _config.GoogleCalendarId;
            checkBoxUseProxy.Checked = _config.UseProxy;
            textBoxProxyServer.Text = _config.ProxyServer;
            textBoxProxyAccount.Text = _config.ProxyAccount;
            textBoxProxyPassword.Text = _config.ProxyPassword;
            checkBoxSyncBody.Checked = _config.SyncBody;
        }

        private async void buttonTest_Click(object sender, EventArgs e)
        {
            buttonTest.Enabled = false;

            try
            {
                _messageText.Clear();
                _messageText.WriteLine("プロキシ越しの接続テスト開始...");
                var configTemp = new Config();
                configTemp.GoogleCalendarId = textBoxGoogleCalendarID.Text;
                configTemp.UseProxy = checkBoxUseProxy.Checked;
                configTemp.ProxyServer = textBoxProxyServer.Text;
                configTemp.ProxyAccount = textBoxProxyAccount.Text;
                configTemp.ProxyPassword = textBoxProxyPassword.Text;
                configTemp.SyncBody = checkBoxSyncBody.Checked;
                // 接続テストなので本日分の予定を取得してみる
                var start = DateTime.Today;
                var end = start.AddDays(1);
                var googleCalendarProvider = await GoogleCalendarProvider.CreateAsync(configTemp);
                await googleCalendarProvider.GetCalendarItemsAsync(start, end);
                _messageText.WriteLine("成功しました");
            }
            catch (Exception ex)
            {
                PrintException(ex);
            }

            buttonTest.Enabled = true;
        }

        private async void buttonOK_Click(object sender, EventArgs e)
        {
            buttonOK.Enabled = false;
            buttonCancel.Enabled = false;

            // awaitでメッセージループに戻ってもこのダイアログボックスが閉じない様にする
            this.DialogResult = DialogResult.None;
 
            try
            {
                _messageText.Clear();
                _messageText.WriteLine("設定を保存しています...");
                try
                {
                    await SaveConfigAsync();
                    _messageText.WriteLine("設定を保存しました");
                    await Task.Delay(3000);
                    DialogResult = DialogResult.OK;
                }
                catch (Exception ex)
                {
                    PrintException(ex);
                }
            }
            catch (Exception ex)
            {
                FormUtil.ShowException(this, ex);
            }

            buttonOK.Enabled = true;
            buttonCancel.Enabled = true;
        }

        private async Task SaveConfigAsync()
        {
            if (_config == default(Config))
            {
                return;
            }
            _config.GoogleCalendarId = textBoxGoogleCalendarID.Text;
            _config.UseProxy = checkBoxUseProxy.Checked;
            _config.ProxyServer = textBoxProxyServer.Text;
            _config.ProxyAccount = textBoxProxyAccount.Text;
            _config.ProxyPassword = textBoxProxyPassword.Text;
            _config.SyncBody = checkBoxSyncBody.Checked;
            var exceptionSave = default(Exception);
            for (int i = 0; i < COUNT_RETRY_LOAD; ++i)
            {
                try
                {
                    _config.Save();
                    exceptionSave = default(Exception);
                }
                catch (Exception ex)
                {
                    exceptionSave = ex;
                }
                await Task.Delay(MS_INTERVAL_RETRY);
            }
            if (exceptionSave != default(Exception))
            {
                throw exceptionSave;
            }
        }

        private void PrintException(Exception ex)
        {
            for (var e = ex; e != null; e = e.InnerException)
            {
                _messageText.WriteLine(e.Message);
            }
        }
    }
}
