﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace gmdofu4common
{
    public class Version
    {
        public class Element
        {
            public readonly string FileName;
            public readonly string Version;

            public Element(string fn, string ver)
            {
                FileName = fn;
                Version = ver;
            }
        };

        private static string[] FILENAMES = {
            "gmdofu4.exe",
            "gmdofu4con.exe",
            "gmdofu4common.dll"
        };

        public static FileVersionInfo[] GetFileVersions()
        {
            var list = new List<FileVersionInfo>(FILENAMES.Length);
            foreach (var fileName in FILENAMES)
            {
                var pathFileName = FileUtil.GetExePathFileName(fileName);
                var verInfo = FileVersionInfo.GetVersionInfo(pathFileName);
                list.Add(verInfo);
            }
            return list.ToArray();
        }

        public static Task<FileVersionInfo[]> GetFileVersionsAsync()
        {
            return Task.Run(() => GetFileVersions());
        }
    }
}
