﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace gmdofu4common
{
    public class FileUtil
    {
        private const string REL_PATH_GMDOFU3 = @"FCT\gmdofu4";

        public static string GetFileDir()
        {
            var pathAppData = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
            var pathGmdofu3 = Path.Combine(pathAppData, REL_PATH_GMDOFU3);
            return pathGmdofu3;
        }

        public static void CreateFileDir()
        {
            var pathGmdofu3 = GetFileDir();
            if (Directory.Exists(pathGmdofu3) == false)
            {
                Directory.CreateDirectory(pathGmdofu3);
            }
        }

        public static string GetFilePath(string fileName)
        {
            var pathGmdofu3 = GetFileDir();
            var pathFileName = Path.Combine(pathGmdofu3, fileName);
            return pathFileName;
        }

        public static string GetExePathFileName(string fileName)
        {
            Assembly asm = Assembly.GetEntryAssembly();
            var pathExe = Path.GetDirectoryName(asm.Location);
            var pathExeFileName = Path.Combine(pathExe, fileName);
            return pathExeFileName;
        }
    }
}
