﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace gmdofu4common
{
    [Serializable]
    public class Config
    {
        // TODO: 設定のロードとセーブが別プロセスで行われることがあるのでファイルアクセスが競合しないようにシリアライズが必要
 
        private const string FILENAME = "config.xml";
        private static XmlSerializer _serializer = new XmlSerializer(typeof(Config));
        private static Encoding _encoding = new UTF8Encoding(false);

        public static Config Load()
        {
            var self = default(Config);
            try
            {
                var pathConfig = FileUtil.GetFilePath(FILENAME);
                using (var r = new StreamReader(pathConfig))
                {
                    self = (Config)_serializer.Deserialize(r);
                }
            }
            catch (Exception)
            {
                self = new Config();
            }
            return self;
        }

        public string GoogleCalendarId = string.Empty;
        public bool UseProxy = false;
        public string ProxyServer = string.Empty;
        public string ProxyAccount = string.Empty;
        public string ProxyPassword = string.Empty;
        public bool SyncBody = true;

        public void Save()
        {
            var pathConfig = FileUtil.GetFilePath(FILENAME);
            using (var w = new StreamWriter(pathConfig, false, _encoding))
            {
                _serializer.Serialize(w, this);
            }
        }
    }
}
