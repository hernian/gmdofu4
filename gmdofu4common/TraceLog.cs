﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace gmdofu4common
{
    public class TraceLog
    {
        private const string NAME_FILE_LOG1 = "gmdofu4.log";
        private const string NAME_FILE_LOG2 = "gmdofu4.log2";
        private const string NAME_MUTEX = "gmdofu4log_mutex";
        private const long SIZE_MAX_LOG = 1024L * 1024L;
        private readonly char[] lineDelimiter = { '\r', '\n' };

        public static TraceLog _theInstance;

        public static TraceLog TheInstance
        {
            get
            {
                if (_theInstance == null)
                {
                    _theInstance = new TraceLog();
                }
                return _theInstance;
            }
        }

        private string _pathLog1;
        private string _pathLog2;
        private FileInfo _fileInfoLog;
        private Mutex _mutex;
        private Encoding _enc;

        private TraceLog()
        {
            _pathLog1 = FileUtil.GetFilePath(NAME_FILE_LOG1);
            _pathLog2 = FileUtil.GetFilePath(NAME_FILE_LOG2);
            _fileInfoLog = new FileInfo(_pathLog1);
            _mutex = new Mutex(false, NAME_MUTEX);
            _enc = new UTF8Encoding(false);
        }

        public string LogFileName
        {
            get { return _pathLog1; }
        }

        public TraceLog WriteLine(string value)
        {
            WriteHarness(sw =>
            {
                var timeStamp = GetTimeStamp();
                sw.WriteLine("{0} {1}", timeStamp, value);
            });
            return this;
        }

        public TraceLog WriteLine(string format, params object[] args)
        {
            var value = string.Format(format, args);
            WriteLine(value);
            return this;
        }

        public TraceLog WriteLine(Exception ex)
        {
            WriteHarness(sw =>
            {
                var timeStamp = GetTimeStamp();
                sw.Write("{0} ", timeStamp);
                while (ex != null)
                {
                    var value = ex.ToString();
                    sw.Write(value);
                    if (value.EndsWith("\n") == false)
                    {
                        sw.WriteLine();
                    }
                    ex = ex.InnerException;
                }
            });
            return this;
        }

        public void WriteCalendarItem(string action, CalendarItem calendarItem)
        {
            var lines = calendarItem.Body.Split(lineDelimiter);
            var body = string.Join("\\n", lines);
            string msg = string.Format("{0} 名前:{1} 開始時刻:{2} 終了時刻:{3} 最終変更:{4} GUID:{5} 場所:{6} 本文:{7}",
                                action,
                                calendarItem.Name,
                                calendarItem.Start.ToString("s"),
                                calendarItem.End.ToString("s"),
                                calendarItem.LastModified.ToString("s"),
                                calendarItem.SyngronizeGuid.ToString("B"),
                                calendarItem.Location,
                                body);
            WriteLine(msg);
        }

        private string GetTimeStamp()
        {
            string timeStamp = DateTime.Now.ToString("s");
            return timeStamp;
        }

        private void WriteHarness(Action<StreamWriter> action)
        {
            _mutex.WaitOne();
            try
            {
                if (File.Exists(_pathLog1))
                {
                    _fileInfoLog.Refresh();
                    if (_fileInfoLog.Length > SIZE_MAX_LOG)
                    {
                        ReplaceLog();
                    }
                }
                using (var sw = new StreamWriter(_pathLog1, true, _enc))
                {
                    action(sw);
                }
            }
            catch (Exception)
            {
                // NOTE:ログ書き込み中に例外が投げられたが、どうしようもないので無視する
            }
            _mutex.ReleaseMutex();
        }

        private void ReplaceLog()
        {
            File.Delete(_pathLog2);
            File.Move(_pathLog1, _pathLog2);
        }
    }

}
