﻿using Google.Apis.Auth.OAuth2;
using Google.Apis.Calendar.v3;
using Google.Apis.Calendar.v3.Data;
using Google.Apis.Services;
using Google.Apis.Util.Store;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace gmdofu4common
{
    public class GoogleCalendarProvider
    {
        public static async Task<GoogleCalendarProvider> CreateAsync(Config config)
        {
            var gcp = new GoogleCalendarProvider(config);
            await gcp.InitializeAsync();
            return gcp;
        }

        private static string[] scopes = { CalendarService.Scope.Calendar };
        private const string APP_NAME = "gmdofu3";
        private const string TARGET_FOR_GET_PROXYHOST = "https://accounts.google.com/";
        private const string FILE_NAME_CLIENT_SECRET = "client_secret.json";
        private const string DIR_NAME_CRDENTIALS = "credentials";

        private const string EX_PROPERTY_NAME_SYNC_GUID = "gmdofu3id";
        private const string TIME_FORMAT_ALL_DAY = "yyyy-MM-dd";
        private const string EVENT_STATUS_CONFIRMED = "confirmed";
        private const string EVENT_STATUS_CANCELLED = "cancelled";

        private TraceLog _log;
        private Config _config;
        private CalendarService _service;

        private GoogleCalendarProvider(Config config)
        {
            _log = TraceLog.TheInstance;
            _config = config;
        }

        private async Task InitializeAsync()
        {
            WebRequest.DefaultWebProxy = null;
            if (_config.UseProxy)
            {
                var webProxy = new WebProxy(_config.ProxyServer);
                if ((string.IsNullOrEmpty(_config.ProxyAccount) == false) || (string.IsNullOrEmpty(_config.ProxyPassword) == false))
                {
                    webProxy.Credentials = new NetworkCredential(_config.ProxyAccount, _config.ProxyPassword);
                }
                WebRequest.DefaultWebProxy = webProxy;
            }

            ClientSecrets secrets = GetClientSecrets();
            var credPath = FileUtil.GetFilePath(DIR_NAME_CRDENTIALS);
            var dataStore = new FileDataStore(credPath, true);
            var credential = await GoogleWebAuthorizationBroker.AuthorizeAsync(secrets, scopes, "user", CancellationToken.None, dataStore).ConfigureAwait(false);
            var initializer = new BaseClientService.Initializer()
            {
                HttpClientInitializer = credential,
                ApplicationName = APP_NAME
            };
            _service = new CalendarService(initializer);
        }

        public async Task<List<CalendarItem>> GetCalendarItemsAsync(DateTime start, DateTime end)
        {
            var calendarItems = new List<CalendarItem>();
            var events = await GetEventsAsync(start, end).ConfigureAwait(false);
            foreach (var ev in events)
            {
                if (ev.Recurrence != null)
                {
                    // 繰り返しイベントは同期の対象外
                    continue;
                }

                var sd = GetDateTime(ev.Start);
                var ed = GetDateTime(ev.End);
                if ((ed <= start) || (end <= sd))
                {
                    // 範囲外は無視する
                    continue;
                }
                var calendarItem = CreateCalendarItem(ev);
                calendarItems.Add(calendarItem);
            }
            return calendarItems;
        }

        public async Task ApplyAsync(List<CalendarItem> list)
        {
            _log.WriteLine("Google更新開始");
            foreach (var calendarItem in list)
            {
                try
                {
                    if (calendarItem.Changed == false)
                    {
                        continue;
                    }
                    if (calendarItem.ID == string.Empty)
                    {
                        await CreateItemAsync(calendarItem).ConfigureAwait(false);
                    }
                    else if (calendarItem.Cancelled)
                    {
                        await DeleteItemAsync(calendarItem).ConfigureAwait(false);
                    }
                    else
                    {
                        await UpdateItemAsync(calendarItem).ConfigureAwait(false);
                    }
                }
                catch (Exception ex)
                {
                    _log.WriteLine(ex);
                }
            }
            _log.WriteLine("Google更新完了");
        }

        private async Task<List<Event>> GetEventsAsync(DateTime start, DateTime end)
        {
            var list = new List<Event>();
            var req = _service.Events.List(_config.GoogleCalendarId);
            req.SingleEvents = false;
            req.ShowDeleted = true;
            req.TimeMin = start;
            req.TimeMax = end;
            // TODO:実験用なので削除すること
            req.ModifyRequest = (rm) => {
                var strAuthValue = string.Format("{0}:{1}", "ishida.haruyuki@jp,fujitsu.com", "4219542195");
                var base64AuthValue = Convert.ToBase64String(Encoding.ASCII.GetBytes(strAuthValue));
                rm.Headers.ProxyAuthorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", base64AuthValue);
            };

            string pageToken = null;
            do
            {
                req.PageToken = pageToken;
                var events = await req.ExecuteAsync().ConfigureAwait(false);
                list.AddRange(events.Items);
                pageToken = events.NextPageToken;
            }
            while (pageToken != null);

            return list;
        }

        private DateTime GetDateTime(EventDateTime evDateTime)
        {
            return evDateTime.DateTime.HasValue ? evDateTime.DateTime.Value : DateTime.ParseExact(evDateTime.Date, TIME_FORMAT_ALL_DAY, null);
        }

        private EventDateTime GetEventDateTime(DateTime dateTime, bool allday)
        {
            var result = new EventDateTime();
            if (allday)
            {
                result.Date = dateTime.ToString(TIME_FORMAT_ALL_DAY);
            }
            else
            {
                result.DateTime = dateTime;
            }
            return result;
        }

        private CalendarItem CreateCalendarItem(Event ev)
        {
            var calendarItem = new CalendarItem();
            calendarItem.ID = ev.Id;
            calendarItem.SyngronizeGuid = GetSynchronizedGuid(ev);
            calendarItem.Name = (ev.Summary != null) ? ev.Summary : string.Empty;
            var a = ev.Start;
            calendarItem.Start = GetDateTime(ev.Start);
            calendarItem.End = GetDateTime(ev.End);
            calendarItem.LastModified = ev.Updated.HasValue ? ev.Updated.Value : new DateTime(0L);
            calendarItem.AllDayEvent = (ev.Start.DateTime.HasValue == false);
            calendarItem.Cancelled = (ev.Status == EVENT_STATUS_CANCELLED);
            calendarItem.Location = (ev.Location != null) ? ev.Location : string.Empty;
            calendarItem.Body = (ev.Description != null) ? ev.Description : string.Empty;
            calendarItem.Tag = ev;
            return calendarItem;
        }

        private Guid GetSynchronizedGuid(Event ev)
        {
            if (ev.ExtendedProperties == null)
            {
                return Guid.Empty;
            }
            if (ev.ExtendedProperties.Private__ == null)
            {
                return Guid.Empty;
            }
            var strGuid = default(string);
            if (ev.ExtendedProperties.Private__.TryGetValue(EX_PROPERTY_NAME_SYNC_GUID, out strGuid) == false)
            {
                return Guid.Empty;
            }
            var guid = default(Guid);
            if (Guid.TryParse(strGuid, out guid) == false)
            {
                return Guid.Empty;
            }
            return guid;
        }

        private async Task CreateItemAsync(CalendarItem calendarItem)
        {
            _log.WriteCalendarItem("新規", calendarItem);

            var ev = new Event();
            if (_config.SyncBody == false)
            {
                // セキュリティの観点からOutlook予定表の本文はGoogleカレンダーに入れない。
                calendarItem.Body = "※Outlook予定表を参照してください。";
            }
            UpdateEvent(ev, calendarItem);
            var req = _service.Events.Insert(ev, _config.GoogleCalendarId);
            await req.ExecuteAsync().ConfigureAwait(false);
        }

        private async Task DeleteItemAsync(CalendarItem calendarItem)
        {
            var ev = calendarItem.Tag as Event;
            if (ev.Status == EVENT_STATUS_CANCELLED)
            {
                // すでにキャンセルされているので何もしない
                return;
            }

            _log.WriteCalendarItem("削除", calendarItem);
            var req = _service.Events.Delete(_config.GoogleCalendarId, ev.Id);
            await req.ExecuteAsync().ConfigureAwait(false);
        }

        private async Task UpdateItemAsync(CalendarItem calendarItem)
        {
            var ev = FindEvent(calendarItem);

            var calendarItemOrg = CreateCalendarItem(ev);
            _log.WriteCalendarItem("変更前", calendarItemOrg);
            _log.WriteCalendarItem("変更後", calendarItem);

            UpdateEvent(ev, calendarItem);
            var req = _service.Events.Update(ev, _config.GoogleCalendarId, ev.Id);
            await req.ExecuteAsync().ConfigureAwait(false);
        }

        private Event FindEvent(CalendarItem calendarItem)
        {
            var ev = calendarItem.Tag as Event;
            if (ev == null)
            {
                var msg = string.Format("Event not found. ID:{0}", calendarItem.ID);
                throw new Exception(msg);
            }
            return ev;
        }

        private void UpdateEvent(Event ev, CalendarItem calendarItem)
        {
            ev.Summary = calendarItem.Name;
            ev.Location = calendarItem.Location;
            ev.Start = GetEventDateTime(calendarItem.Start, calendarItem.AllDayEvent);
            ev.End = GetEventDateTime(calendarItem.End, calendarItem.AllDayEvent);
            if (ev.Status != EVENT_STATUS_CONFIRMED)
            {
                ev.Status = EVENT_STATUS_CONFIRMED;
            }
            ev.Description = calendarItem.Body;
            if (ev.ExtendedProperties == null)
            {
                ev.ExtendedProperties = new Event.ExtendedPropertiesData();
            }
            if (ev.ExtendedProperties.Private__ == null)
            {
                ev.ExtendedProperties.Private__ = new Dictionary<string, string>();
            }
            ev.ExtendedProperties.Private__[EX_PROPERTY_NAME_SYNC_GUID] = calendarItem.SyngronizeGuid.ToString();
            ev.Reminders = new Event.RemindersData();
            ev.Reminders.UseDefault = false;
        }

        private ClientSecrets GetClientSecrets()
        {
            var pahClientSecret = FileUtil.GetExePathFileName(FILE_NAME_CLIENT_SECRET);
            using (var stream = new FileStream(pahClientSecret, FileMode.Open, FileAccess.Read))
            {
                var clientSecrets = GoogleClientSecrets.Load(stream).Secrets;
                return clientSecrets;
            }
        }

    }
}
